# MongoDB / Mongoose forum backend

This is a forum backend that is supposed to work a bit like reddit.

### Installing

Run `npm install` in the root directory.

### Running the app

The backend can be started with `npm start`. It will start listening and serve your requests.

### Running the tests

To run all tests use `npm test`. There are three kinds of tests:

- unit tests on the schemas
- integration tests on the endpoints
- system tests by walking through a 'user journey'
