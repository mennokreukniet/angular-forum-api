const express = require('express')
const router = express.Router()

const User = require('../models/user.model')() // note we need to call the model caching function

const duplicateMiddleware = require('../middleware/verifyDuplicate')
const CrudController = require('../controllers/crud')
const UserCrudController = new CrudController(User)
const SubController = require('../controllers/sub.controller')

const AuthController = require('../controllers/auth.controller')

// get all users
router.get('/', UserCrudController.getAll)

// get a user
router.get('/:id', UserCrudController.getOne)

// update a user
router.put('/:id', UserCrudController.update)

// remove a user
router.delete('/:id', AuthController.deleteUser)

// login
router.post('/login', AuthController.login)

// register
router.post('/register', duplicateMiddleware.checkDuplicateEmail, duplicateMiddleware.checkDuplicateUsername, AuthController.register)

// get followed subs
router.get('/:id/followed', SubController.getFollowedSubs)

module.exports = router