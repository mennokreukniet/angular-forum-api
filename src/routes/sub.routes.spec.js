const chai = require('chai')
const expect = chai.expect
const jwt = require("jsonwebtoken");

const requester = require('../../requester.spec')

const User = require('../models/user.model')() 
const Post = require('../models/post.model')() 
const Comment = require('../models/comment.model')()
const Sub = require('../models/sub.model')()

const chaiHttp = require('chai-http')
const server = require('../../connect')

chai.should()
chai.use(chaiHttp)


describe('Sub create tests (POST /sub) ', () => {
    it('Should create a new sub when user is logged in.', async function() {
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "AssholeDesign",
            about: "Because nothing comes before profit, especially not the consumer.",
            rules: "1. Don’t be Subtle. Please make it obvious what you’re trying to highlight. A clear and concise post title can go a long way.",
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));


        expect(subRes).to.have.status(201)
        expect(subRes.body).to.have.property('message')
        expect(subRes.body).to.have.property('result')

    })

    it('Should NOT create a new sub when user is NOT logged in.', async function() {
        const subRes = await requester.post('/sub').send({
            name: "AssholeDesign23",
            about: "Because nothing comes before profit, especially not the consumer.",
            rules: "1. Don’t be Subtle. Please make it obvious what you’re trying to highlight. A clear and concise post title can go a long way.",
            admin: ""
        })

        expect(subRes).to.have.status(401)
    })
})



describe('GET All subs tests (GET /sub) ', () => {
    it('Should get all subforums', async function() {
        const res = await requester.get('/sub').send()

        expect(res).to.have.status(200)
        expect(res.body).to.be.an('array')
    })
})

describe('GET All posts from a subforum tests (GET /sub/:id/posts) ', () => {

    it('Should get all posts from a subforum', async function() {
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "OldSchoolCool",
            about: "History's cool kids, looking fantastic",
            rules: "1. Must be 25 years or older. 2. Please make it obvious what you’re trying to highlight. A clear and concise post title can go a long way.",
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "test post",
            text: "i am the test post",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 720,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes2 = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "second test post",
            text: "i am the second test post",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 720,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const res = await requester.get('/sub/' + loginRes.body._id + '/posts').send()

        expect(res).to.have.status(200)
        expect(res.body.posts).to.be.an('array')
    })
})

describe('GET One Subforum tests (GET /sub/:id) ', () => {
    it('Should get one subforum', async function() {

        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "WW1Pics",
            about: "World War One pictures",
            rules: "1. Must be a world war one era picture",
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const res = await requester.get('/sub/' + subRes.body.result._id)

        expect(res).to.have.status(200)
        expect(res.body).to.be.an('object')
    })
})


describe('Edit a Subforum tests (GET /sub/:id) ', () => {
    it('Should edit one subforum', async function() {
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "Movies",
            about: "News & Discussion about Major Motion Pictures",
            rules: "1. aidiascdnv",
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const editRes = await requester.put('/sub/' + subRes.body.result._id).send({
            name: "BikiniBottomTwitter"
        }).set("authorization", "Bearer " + jwt.sign({ userId: loginRes.body._id }, "longer-secret-is-better"));

        expect(editRes).to.have.status(200)
    })
})