const express = require('express')
const router = express.Router()
const duplicateMiddleware = require("../middleware/verifyDuplicate");
const Sub = require('../models/sub.model')() 
const CrudController = require('../controllers/crud')
const validateMiddleware =  require('../middleware/validateToken')
const SubCrudController = new CrudController(Sub)
const SubController = require('../controllers/sub.controller')
const AuthController = require('../controllers/auth.controller')

// create subforum
router.post('/', validateMiddleware.validateToken, duplicateMiddleware.checkDuplicateSub, SubController.createSub)

// get all subforums
router.get('/', SubCrudController.getAll)

// get all posts from subforum
router.get('/:id/posts', SubController.getPosts)

router.get('/:id', SubCrudController.getOne)

router.put('/:id', validateMiddleware.validateToken, SubController.update)

router.delete('/:id', validateMiddleware.validateToken, SubController.deleteSub)

router.post('/:id/follow', validateMiddleware.validateToken, SubController.followSub)

router.post('/:id/unfollow', validateMiddleware.validateToken, SubController.unfollowSub)

module.exports = router