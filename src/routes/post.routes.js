const express = require('express')
const router = express.Router()

const Post = require('../models/post.model')() 

const CrudController = require('../controllers/crud')
const PostCrudController = new CrudController(Post)

const duplicateMiddleware = require('../middleware/verifyDuplicate')
const validateMiddleware =  require('../middleware/validateToken')
const PostController = require('../controllers/post.controller')
const AuthController = require('../controllers/auth.controller')
const CommentController = require('../controllers/comment.controller')

// post on a sub
router.post('/sub/:id/createPost', validateMiddleware.validateToken, PostController.create)

// get all posts ever
router.get('/', PostCrudController.getAll)

// get one post
router.get('/:id', PostCrudController.getOne)

// edit post
router.put('/:id', validateMiddleware.validateToken, PostController.update)

// delete post
router.delete('/:id', validateMiddleware.validateToken, PostController.deletePost)

// get comments of post by id
router.get('/:id/comments', CommentController.getComments)

// create comment
router.post('/:id/createComment', validateMiddleware.validateToken, CommentController.createComment)

// update comment
router.put('/:id/updateComment/:commentId', validateMiddleware.validateToken, CommentController.updateComment)

// delete comment
router.delete('/:id/deleteComment/:commentId', validateMiddleware.validateToken, CommentController.deleteComment)

module.exports = router