const chai = require('chai')
const expect = chai.expect
const jwt = require("jsonwebtoken");

const requester = require('../../requester.spec')

const User = require('../models/user.model')() 
const Post = require('../models/post.model')() 
const Comment = require('../models/comment.model')()
const Sub = require('../models/sub.model')()

const chaiHttp = require('chai-http')
const server = require('../../connect')

chai.should()
chai.use(chaiHttp)

describe('Post create tests (POST /post) ', () => {
    before((done) => {
        server.emptyCollection('subs')
        server.emptyCollection('posts')
        server.emptyCollection('comments')
        done()
    })

    it('should create a new post', async function() {

        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "AskEveryone",
            about: "Ask everyone on this subforum a question",
            rules: "1. dont post images 2. dont be offensive",
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const res = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "test post",
            text: "i am the test post",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 720,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        expect(res).to.have.status(201)
        expect(res.body).to.have.property('message')
        expect(res.body).to.have.property('result')

        const post = await Post.findOne({title: 'test post'})
        expect(post).to.have.property('title', 'test post')
    })

    it('should not create post if a title is not provided', async function() {
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "uniquetestname2222",
            about: "Ask everyone on this subforum a question",
            rules: "1. dont post images 2. dont be offensive",
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const res = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            text: "i am the test post",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 720,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        // model validation gives 500 error
        expect(res).to.have.status(500)
    })

    it('should not create post if text is not provided', async function() {
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "uniquenametetssub",
            about: "Ask everyone on this subforum a question",
            rules: "1. dont post images 2. dont be offensive",
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const res = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "testpost",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 720,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        // model validation gives 500 error
        expect(res).to.have.status(500)
    })

    it('should not create a new post when not logged in', async function() {

        const res = await requester.post('/post/sub/noid/createPost').send({
            title: "test post",
            text: "i am the test post",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 720,
            originalPoster: 1
        })

        expect(res).to.have.status(401)
        expect(res.body).to.have.property('error')
        expect(res.body).to.have.property('datetime')
    })
})


describe('Get Posts tests (GET /post/[?]) ', () => {
    before((done) => {
        server.emptyCollection('subs')
        done()
    })
    
    it('Should get all posts', async function() {

        const res = await requester.get('/post').send()
        expect(res).to.have.status(200)
        expect(res.body).to.be.an('array')
    })

    it('Should get one post', async function() {

        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "newsub",
            about: "no thing",
            rules: "1. yers 2. no",
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "test post agiain",
            text: "i am the test post again",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 234,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const res = await requester.get('/post/' + postRes.body.result._id).send()

        expect(res).to.have.status(200)
        expect(res.body).to.be.an('object')
        expect(res.body).to.have.property('title')
    })
})

describe('Edit Post tests (PUT /post/:id) ', () => {
    it('Should edit the post successfully', async function() {
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "testsub2",
            about: "test data",
            rules: ["1. dont post hate speech", "2. dont be offensive"],
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "test post before edit",
            text: "before edit",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 423,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const res = await requester.put('/post/' + postRes.body.result._id).send({
            title: "edited test post",
            text: "i am the test post again, but edited",
            likes: 354,
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        expect(res).to.have.status(200)
        expect(res.body).to.be.an('object')
        expect(res.body).to.have.property('message')
    })

    it('Should return an error if not logged in.', async function() {
        const res = await requester.put('/post/1').send({
            title: "edited test post",
            text: "i am the test post again, but edited",
            likes: 354,
        })

        expect(res).to.have.status(401)
        expect(res.body).to.be.an('object')
        expect(res.body).to.have.property('error')
        expect(res.body).to.have.property('datetime')
    })

    it('Should return an error if resource id does not exist.', async function() {

        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const res = await requester.put('/post/200321').send({
            title: "nonexistantpost",
            text: "i am the test post again, but i dont exist",
            likes: 999999,
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        expect(res).to.have.status(400)
        expect(res.body).to.be.an('object')
        expect(res.body).to.have.property('message')
    })

    it('Should return an error if request body is empty.', async function() {

        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "testsub3",
            about: "test data",
            rules: ["1. dont post hate speech", "2. dont be offensive"],
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "test post before edit",
            text: "before edit",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 423,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const res = await requester.put('/post/' + postRes.body.result_id).send().set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        expect(res).to.have.status(400)
        expect(res.body).to.be.an('object')
        expect(res.body).to.have.property('message')
    })


})


describe('Delete Post tests (DELETE /post/:id) ', () => {
    it('Should delete a post if the user is the original poster', async function(){
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "shitposting",
            about: "post whatever you want",
            rules: ["1. no rules"],
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "tobedeleted",
            text: "before delete",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 1639,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));


        const postId = postRes.body.result._id

        const deleteRes = await requester.delete('/post/' + postId).send().set("authorization", "Bearer " + jwt.sign({ userId: loginRes.body._id }, "longer-secret-is-better"))

        expect(deleteRes).to.have.status(200)
        expect(deleteRes.body).to.be.an('object')
        expect(deleteRes.body).to.have.property('message')
    })

    it('Should NOT delete a post if the user is NOT the original poster', async function(){
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "AmA",
            about: "Ask me Anything",
            rules: ["1. dont post hate speech", "2. dont be offensive"],
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "tobedeleted",
            text: "before delete",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 1639,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));


        const postId = postRes.body.result._id

        const deleteRes = await requester.delete('/post/' + postId).send().set("authorization", "Bearer " + jwt.sign({ userId: "db1273y871gddjas01908" }, "longer-secret-is-better"))

        expect(deleteRes).to.have.status(401)
    })
})

describe('Get Comments from post tests (GET /post/:id/comments) ', () => {

    it('Should get comments from post.', async function(){
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "TheLastAirbender",
            about: "The subreddit for fans of Avatar: The Last Airbender",
            rules: ["1. dont post hate speech", "2. dont be offensive"],
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "The Cabbages must have their revenge.",
            text: "huhuhu",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 293,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const comment1 = await requester.post('/post/' + postRes.body.result._id + '/createComment').send({
            text: "this is my favorite scene",
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const comment2 = await requester.post('/post/' + postRes.body.result._id + '/createComment').send({
            text: "mine too!",
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));


        const getComments = await requester.get('/post/' + postRes.body.result._id + '/comments').send()

        
        expect(getComments).to.have.status(200)
        expect(getComments.body).to.be.an('array')
    })
})


describe('Create Comment on post tests (POST /post/:id/createComment) ', () => {

    it('Should create a comment if the user is logged in.', async function(){
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "ik_ihe",
            about: "ik in het echte leven",
            rules: ["1. alleen nederlandstalige posts", "2. dont be offensive"],
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "ik_ihe",
            text: "echt hoor",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 293,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const commentRes = await requester.post('/post/' + postRes.body.result._id + '/createComment').send({
            text: "avondklok alweer verlengd"
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));


        expect(commentRes).to.have.status(201)
        expect(commentRes.body).to.be.an('object')
        expect(commentRes.body).to.have.property('message')
        expect(commentRes.body).to.have.property('result')
    })

    it('Should NOT create a comment if the user is NOT logged in.', async function(){
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "valheim",
            about: "subforum for the popular coop rpg valheim.",
            rules: ["1. only posts related to Valheim", "2. dont be offensive"],
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "valheim is great",
            text: "really",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 2378,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        // note I do not send the auth header so we are not logged in
        const commentRes = await requester.post('/post/' + postRes.body.result._id + '/createComment').send({
            text: "avondklok alweer verlengd"
        })

        expect(commentRes).to.have.status(401)
    })

    it('Should throw an error when request body is empty.', async function(){
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "TechSupportGore",
            about: "You will cringe to the brink of passing out after a few minutes looking at this subreddit.",
            rules: ["1. dont post hate speech", "2. dont be offensive"],
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "What could go wrong if I “fixed” this broken phone?",
            text: "stupid",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 156,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const commentRes = await requester.post('/post/' + postRes.body.result._id + '/createComment').send({}).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        // 500 model validation error
        expect(commentRes).to.have.status(500)
    })
})

describe('Edit Comment on post tests (PUT /:id/updateComment/:commentId) ', () => {

    it('Should update a comment if the user is logged in and the originalposter.', async function() {
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "KidsAreFuckingStupid",
            about: "kids, who are not smart",
            rules: ["1. dont post hate speech", "2. dont be offensive"],
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "this kid is dumb as rocks",
            text: "dont u agree?",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 293,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const commentRes = await requester.post('/post/' + postRes.body.result._id + '/createComment').send({
            text: "yeah that was really stupid even for a kid."
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));


        const editRes = await requester.put('/post/' + postRes.body.result._id + '/updateComment/' + commentRes.body.result._id).send({
            text: "yeah that was really stupid even for a kid. Edit: edited post"
        }).set("authorization", "Bearer " + jwt.sign({ userId: loginRes.body._id }, "longer-secret-is-better"));


        expect(editRes).to.have.status(200)
        expect(editRes.body).to.be.an('object')
        expect(editRes.body).to.have.property('message')
    })

    it('Should NOT update a comment if the user is NOT logged in.', async function() {
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "2007scape",
            about: "subforum for the mmorpg old school runescape ",
            rules: ["1. Body of submission must be related to osrs", "2. dont be offensive"],
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "REEEE",
            text: "Mods cant stop us",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 5000,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const commentRes = await requester.post('/post/' + postRes.body.result._id + '/createComment').send({
            text: "REEEEEEEEEEEEEEEEEEEEE"
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));


        const editRes = await requester.put('/post/' + postRes.body.result._id + '/updateComment/' + commentRes.body.result._id).send({
            text: "REEEEEEEEEEEEEEEEEEEEE Edit: this edit won't go through"
        })


        expect(editRes).to.have.status(401)
    })

    it('Should NOT update a comment if the user is NOT the original poster.', async function() {
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "196",
            about: "Be sure to check the 1 rule on your way out",
            rules: ["1. If you visit this subreddit, you must post before you leave."],
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "Snakes if they had massive balls but 3D but it's booba",
            text: "title",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 3000,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const commentRes = await requester.post('/post/' + postRes.body.result._id + '/createComment').send({
            text: "BOOBA"
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));


        // note: the id I set here is not the originalposters'
        const editRes = await requester.put('/post/' + postRes.body.result._id + '/updateComment/' + commentRes.body.result._id).send({
            text: "REEEEEEEEEEEEEEEEEEEEE Edit: this edit won't go through"
        }).set("authorization", "Bearer " + jwt.sign({ id: "12nnd8137hal1029dns" }, "longer-secret-is-better"));


        expect(editRes).to.have.status(401)
    })
})


describe('Delete Comment on post tests (PUT /:id/deleteComment/:commentId) ', () => {

    it('Should delete a comment if the user is logged in and the originalposter.', async function() {
        
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "RedNeckEngineering",
            about: "yurp",
            rules: ["1. dont post hate speech", "2. dont be offensive"],
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "redneck unloading",
            text: "stupid",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 345,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const commentRes = await requester.post('/post/' + postRes.body.result._id + '/createComment').send({
            text: "what could go wrong"
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));


        const deleteCommentRes = await requester.delete('/post/' + postRes.body.result._id + '/deleteComment/' + commentRes.body.result._id).send().set("authorization", "Bearer " + jwt.sign({ userId: loginRes.body._id }, "longer-secret-is-better"));

        expect(deleteCommentRes).to.have.status(200)
        expect(deleteCommentRes.body).to.be.an('object')
        expect(deleteCommentRes.body).to.have.property('message')
    })

    it('Should NOT delete a comment if the user is NOT logged in.', async function() {
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "Photoshopbattles",
            about: "battles in photoshop",
            rules: ["1. dont post hate speech", "2. dont be offensive"],
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "scared cat",
            text: "like the",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 293,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const commentRes = await requester.post('/post/' + postRes.body.result._id + '/createComment').send({
            text: "dimensional cat [url]"
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));


        const deleteCommentRes = await requester.delete('/post/' + postRes.body.result._id + '/deleteComment/' + commentRes.body.result._id).send()

        expect(deleteCommentRes).to.have.status(401)
    })

    it('Should NOT delete a comment if the user is logged in and NOT the originalposter.', async function() {
        
        const loginRes = await requester.post('/user/login').send({
            email: 'semjensen@test.nl',
            password: 'secret',
        })

        const subRes = await requester.post('/sub').send({
            name: "Funny",
            about: "Welcome to r/Funny, Reddit's largest humour depository.",
            rules: ["1. All posts must make an attempt at homour.", "2. dont be offensive"],
            admin: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const postRes = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send({
            title: "mr steal yo girl",
            text: "title",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            likes: 421,
            originalPoster: loginRes.body._id
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

        const commentRes = await requester.post('/post/' + postRes.body.result._id + '/createComment').send({
            text: "comment text"
        }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));


        const deleteCommentRes = await requester.delete('/post/' + postRes.body.result._id + '/deleteComment/' + commentRes.body.result._id).send().set("authorization", "Bearer " + jwt.sign({ userId: "anjdvcusadn2877237nbcsas" }, "longer-secret-is-better"));

        expect(deleteCommentRes).to.have.status(401)
    })

})