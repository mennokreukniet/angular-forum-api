const jwt = require("jsonwebtoken");

function validateToken(req, res, next) {
    // The headers should contain the authorization-field with value 'Bearer [token]'
    const authHeader = req.headers.authorization
    
    if (!authHeader) {
        res.status(401).json({
            error: 'Authorization header missing!',
            datetime: new Date().toISOString()
        })
    }else   {
        // Strip the word 'Bearer' from the header value
        const token = authHeader.substring(7, authHeader.length)

        jwt.verify(token, 'longer-secret-is-better', (err, payload) => {
            if (err) {
                res.status(401).json({
                    error: 'Not authorized',
                    datetime: new Date().toISOString()
                })
            }
            if (payload) {

                // User heeft toegang. Voeg UserId uit payload toe aan
                // request, voor ieder volgend endpoint.
                req.userId = payload.userId
                next()
            }
        })
    }
}

module.exports = {
    validateToken
}