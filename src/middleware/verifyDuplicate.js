const Post = require('../models/post.model')() 
const Sub = require('../models/sub.model')() 
const User = require('../models/user.model')() 


checkDuplicateEmail = (req, res, next) => {
    User.findOne({
      email: req.body.email
    }).exec((err, user) => {
      if (err) {
        res.status(500).send({ message: "Email is already in use!" })
        return
      }
      if (user) {
        res.status(400).send({ message: "Email is already in use!" })
        return
      }
      next()
    })
}

checkDuplicateUsername = (req, res, next) => {
    User.findOne({
      username: req.body.username
    }).exec((err, user) => {
      if (err) {
        res.status(500).send({ message: "Username is already in use!" })
        return
      }
      if (user) {
        res.status(400).send({ message: "Username is already in use!" })
        return
      }
      next()
    })
}

checkDuplicateSub = (req, res, next) => {
    Sub.findOne({
      name: req.body.name
    }).exec((err, user) => {
      if (err) {
        res.status(500).send({ message: "Subforum name is already in use!" })
        return
      }
      if (user) {
        res.status(400).send({ message: "Subforum is already in use!" })
        return
      }
      next()
    })
}

module.exports = {
    checkDuplicateEmail,
    checkDuplicateSub,
    checkDuplicateUsername,
}