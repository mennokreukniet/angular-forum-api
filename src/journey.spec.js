const chai = require('chai')
const expect = chai.expect

const requester = require('../requester.spec')
const jwt = require("jsonwebtoken");

describe('user journey', function() {
    it('register user; login user; user creates subforum; user creates post;  user 2 registers; user 2 logs in; user 2 comments on users post ', async function()    {
        let res
        
        res = await requester.post('/user/register').send({
            username: 'menno',
            email: 'test@menno.nl',
            password: 'secret',
            birthdate: '1999-08-10'
        })
        expect(res).to.have.status(201)

        res = await requester.post('/user/login').send({
            email: 'test@menno.nl',
            password: 'secret',
        })
        expect(res).to.have.status(200)

        const loginRes =  res;

        const testSub = {
            name: "interestingsub",
            about: "so interesting",
            rules: ["1. dont post images", "2. dont be offensive"],
            admin: loginRes.body._id
        }

        res = await requester.post('/sub').send(testSub).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));
        expect(res).to.have.status(201)

        const subRes = res;

        expect(res.body.result).to.have.property('name', testSub.name)
        expect(res.body.result).to.have.property('about', testSub.about)
        expect(res.body.result.rules).to.be.an('array')
        expect(res.body.result).to.have.property('admin', testSub.admin)
        expect(res.body.result.admin.toString()).to.equal(loginRes.body._id)

        const testPost = {
            title: "interesting post",
            text: "i am the interesting post",
            imagelink: "https://imgur.com/gallery/bGNkORW",
            hyperlink: "https://www.google.com",
            originalPoster: loginRes.body._id,
        }

        res = await requester.post('/post/sub/' + subRes.body.result._id + '/createPost').send(testPost).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));
        expect(res).to.have.status(201)

        expect(res.body.result).to.have.property('title', testPost.title)
        expect(res.body.result).to.have.property('text', testPost.text)
        expect(res.body.result).to.have.property('imagelink', testPost.imagelink)
        expect(res.body.result).to.have.property('hyperlink', testPost.hyperlink)
        expect(res.body.result).to.have.property('originalPoster', testPost.originalPoster)
        expect(res.body.result.originalPoster.toString()).to.equal(loginRes.body._id)

        const postRes = res

        res = await requester.post('/user/register').send({
            username: 'nietmenno',
            email: 'test@nietmenno.nl',
            password: 'secret',
            birthdate: '1999-08-1'
        })
        expect(res).to.have.status(201)

        res = await requester.post('/user/login').send({
            email: 'test@nietmenno.nl',
            password: 'secret',
        })
        expect(res).to.have.status(200)

        const loginRes2 = res

        const testComment = {
            text: "avondklok naar 10u!"
        }

        res = await requester.post('/post/' + postRes.body.result._id + '/createComment').send(testComment).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));
        expect(res).to.have.status(201)
        expect(res.body.result).to.have.property('text', testComment.text)


        res = await requester.get('/post/' + postRes.body.result._id + '/comments').send()

        expect(res).to.have.status(200)
        expect(res.body[0].text).to.equal(testComment.text)
    })
})