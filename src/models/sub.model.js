const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const getModel = require('./model_cache')

// we use the post schema in the product schema
const PostSchema = require('./post.model').schema;

// define the sub schema
const SubSchema = new Schema({
    name: {
        type: String,
        required: [true, 'A subforum needs a name.'],
        unique: [true, 'Name is already taken.']
    },
    about: {
        type: String,
        required: [true, 'A subforum needs an explanation']
    },
    rules: {
        //type: [String],
        type: String,
        required: [true, 'A subforum needs to have rules.']
    },
    admin: {
        type: Schema.Types.ObjectId,
        ref: 'user',
    }
}, {
    // include virtuals when serializing the schema to an object or JSON
    toObject: {virtuals: true},
    toJSON: {virtuals: true},
});

// export the sub model through a caching function
module.exports = getModel('Sub', SubSchema)