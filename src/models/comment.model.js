const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const getModel = require('./model_cache')

// define the sub schema
const CommentSchema = new Schema({
    text: {
        type: String,
        required: [true, 'Text is required.']
    },
    likes: Number,
    creationdate: Date,
    post: {
        type: Schema.Types.ObjectId,
        ref: 'post',
    },
    originalPoster: {
        type: Schema.Types.ObjectId,
        ref: 'user',
    },
});

module.exports = getModel('Comment', CommentSchema)
