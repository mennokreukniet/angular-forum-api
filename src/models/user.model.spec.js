const chai = require('chai')
const expect = chai.expect

const requester = require('../../requester.spec')
const server = require('../../connect')

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const User = require('./user.model')() // note we need to call the model caching function

describe('user model', function() {
    describe('unit tests', function() {
        // before((done) => {
        //     server.emptyCollection('users')
        //     done()
        // })
    
        it('should reject a missing username', async function() {
            const user = new User({
                email: 'test@sem.com',
                password: 'secret',
                birthdate: '1998-08-2'
            })
    
            await expect(user.save()).to.be.rejectedWith(Error)
        })

        it('should reject a missing email', async function() {
            const user = new User({
                username: 'newuser1',
                password: 'secret',
                birthdate: '1994-08-2'
            })
    
            await expect(user.save()).to.be.rejectedWith(Error)
        })

        it('should reject a missing password', async function() {
            const user = new User({
                username: 'newuser1',
                email: 'mail@outlook.com',
                birthdate: '1994-08-2'
            })
    
            await expect(user.save()).to.be.rejectedWith(Error)
        })

        it('should reject a missing birthdate', async function() {
            const user = new User({
                username: 'newuser1',
                email: 'mail@outlook.com',
                password: 'secret'
            })
    
            await expect(user.save()).to.be.rejectedWith(Error)
        })
    })
})