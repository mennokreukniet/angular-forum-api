const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const getModel = require('./model_cache')

// we use the user schema in the product schema
const UserSchema = require('./user.model').schema;

// define the sub schema
const PostSchema = new Schema({
    title: {
        type: String,
        required: [true, 'Title is required.']
    },
    text: {
        type: String,
        required: [true, 'Text is required.']
    },
    imagelink: String,
    hyperlink: String,
    creationdate: Date,
    likes: Number,
    originalPoster: {
        type: Schema.Types.ObjectId,
        ref: 'user',
    },
    sub: {
        type: Schema.Types.ObjectId,
        ref: 'sub',
    },
});

module.exports = getModel('Post', PostSchema)
