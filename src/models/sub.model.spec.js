const chai = require('chai')
const expect = chai.expect
const requester = require('../../requester.spec')
const jwt = require("jsonwebtoken");

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Sub = require('./sub.model')()

describe('sub model', function() {
    describe('unit tests', function() {
        it('should reject a missing sub name', async function() {
            const loginRes = await requester.post('/user/login').send({
                email: 'semjensen@test.nl',
                password: 'secret',
            })

            const sub = new Sub({
                about: "Because nothing comes before profit, especially not the consumer.",
                rules: ["1. dont post hate speech", "2. dont be offensive"],
                admin: loginRes.body._id
            })
    
            await expect(sub.save()).to.be.rejectedWith(Error)
        })

        it('should reject a duplicate sub name', async function() {
            const loginRes = await requester.post('/user/login').send({
                email: 'semjensen@test.nl',
                password: 'secret',
            })
    
            const sub1 = await requester.post('/sub').send({
                name: "dupe",
                about: "Because nothing comes before profit, especially not the consumer.",
                rules: "1. Don’t be Subtle. Please make it obvious what you’re trying to highlight. A clear and concise post title can go a long way.",
                admin: loginRes.body._id
            }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));
    
            const sub2 = await requester.post('/sub').send({
                name: "dupe",
                about: "Because nothing comes before profit, especially not the consumer.",
                rules: "1. Don’t be Subtle. Please make it obvious what you’re trying to highlight. A clear and concise post title can go a long way.",
                admin: loginRes.body._id
            }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

            expect(sub2).to.have.status(400)
        })

        it('should reject a missing sub about section', async function() {
            const loginRes = await requester.post('/user/login').send({
                email: 'semjensen@test.nl',
                password: 'secret',
            })

            const sub = new Sub({
                name: "ShowerThoughts",
                rules: ["1. dont post hate speech", "2. dont be offensive"],
                admin: loginRes.body._id
            })
    
            await expect(sub.save()).to.be.rejectedWith(Error)
        })
    })
})