const chai = require('chai')
const expect = chai.expect
const jwt = require("jsonwebtoken");
const requester = require('../../requester.spec')

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Post = require('./post.model')()


describe('post model', function() {
    describe('unit tests', function() {
        it('should reject a missing post title', async function() {
            const loginRes = await requester.post('/user/login').send({
                email: 'semjensen@test.nl',
                password: 'secret',
            })

            const sub = await requester.post('/sub').send({
                name: "pcmasterrace",
                about: "Because pc is the best",
                rules: "1. Don’t be Subtle. Please make it obvious what you’re trying to highlight. A clear and concise post title can go a long way.",
                admin: loginRes.body._id
            }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

            const post = new Post({
                text: "i have no title ",
                imagelink: "https://imgur.com/gallery/bGNkORW",
                hyperlink: "https://www.google.com",
                likes: 0,
                originalPoster: loginRes.body._id,
                sub: sub.body._id
            }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));
    
            await expect(post.save()).to.be.rejectedWith(Error)
        })

        it('should reject a missing post text', async function() {
            const loginRes = await requester.post('/user/login').send({
                email: 'semjensen@test.nl',
                password: 'secret',
            })

            const sub = await requester.post('/sub').send({
                name: "uniquename",
                about: "Because pc is the best",
                rules: "1. Don’t be Subtle. Please make it obvious what you’re trying to highlight. A clear and concise post title can go a long way.",
                admin: loginRes.body._id
            }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));

            const post = new Post({
                title: "i have no text",
                imagelink: "https://imgur.com/gallery/bGNkORW",
                hyperlink: "https://www.google.com",
                likes: 0,
                originalPoster: loginRes.body._id,
                sub: sub.body._id
            }).set("authorization", "Bearer " + jwt.sign({ id: loginRes.body._id }, "longer-secret-is-better"));
    
            await expect(post.save()).to.be.rejectedWith(Error)
        })
    })
})