const mongoose = require('mongoose');
const Schema = mongoose.Schema
var validate = require('mongoose-validator')

const getModel = require('./model_cache')

const UserSchema = new Schema({
    username: {
        type: String,
        required: [true, 'Username is required.'],
        unique: [true, 'A user needs to have a unique name'],
        // validator: usernameValidator
    },

    email: {
        type: String,
        required: [true, 'E-mail is required.'],
        unique: [true, 'A user needs to have an unique e-mail adress.'],
    },

    birthdate: {
        type: Date,
        required: [true, 'Date of birth is required.'],
    },

    password: {
        type: String,
        required: [true, 'Password is required.']
    },
})

// mongoose plugin to always populate fields
UserSchema.plugin(require('mongoose-autopopulate'));

// export the user model through a caching function
module.exports = getModel('User', UserSchema)