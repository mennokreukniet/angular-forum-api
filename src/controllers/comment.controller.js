const Comment = require('../models/comment.model')() 
const jwt = require("jsonwebtoken")

const errors = require('../errors')

function getComments(req, res){

    Comment.find((error, response) => {
        var responseFiltered = [];

        for (i = 0; i < response.length; i++) {
            if(response[i].post == req.params.id){
                responseFiltered.push(response[i])
            }
        }

        if (error) {
            return next(error)
        } else {
            res.status(200).json(responseFiltered)
        }
    })
    
}

function createComment(req, res) {
    if(!req.body){
        res.status(400).end()
    }

    const authHeader = req.headers.authorization

    const token = authHeader.substring(7, authHeader.length)

    var decoded = jwt.decode(token, {complete: true});

    const newComment = new Comment({
        text: req.body.text,
        likes: 0,
        post: req.params.id,
        originalPoster: decoded.payload.id
    });

    newComment.save().then((response) => {
        res.status(201).json({
            message: "Comment successfully created!",
            result: response
        });
    }).catch(error => {
        res.status(500).json({
            error: error
        });
    });
}

async function updateComment (req, res) {
    const authHeader = req.headers.authorization

    const token = authHeader.substring(7, authHeader.length)

    var decoded = jwt.decode(token, {complete: true});

    const comment = await Comment.findById(req.params.commentId)
    
    if(comment.originalPoster == decoded.payload.userId){
        await Comment.findByIdAndUpdate(req.params.commentId, req.body)
        res.status(200).json({
            message: "Comment successfully edited!",
        });
    }else{
        res.status(401).end()
    }
}

async function deleteComment (req, res) {
    const authHeader = req.headers.authorization

    const token = authHeader.substring(7, authHeader.length)

    var decoded = jwt.decode(token, {complete: true});

    var comment = await Comment.findById(req.params.commentId)

    if(!comment){
        res.status(400).json({
            message: "Invalid resource id: the resource you are looking for does not exist."
        })
    }

    if(comment.originalPoster == decoded.payload.userId) {
        await comment.delete()
        res.status(200).json({
            message: "Comment successfully deleted!",
        });
    }else {
        res.status(401).end()
    }
}

module.exports = {
    getComments,
    createComment,
    updateComment,
    deleteComment
}