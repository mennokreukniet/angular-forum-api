const userModel = require('../models/user.model')() 
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const neo = require("../../neo");

const errors = require('../errors')

function register(req, res){
    bcrypt.hash(req.body.password, 10).then((hash) => {
        const user = new userModel({
            username: req.body.username,
            email: req.body.email,
            birthdate: req.body.birthdate,
            password: hash
        });
        user.save().then((response) => {
            registerNeo4j(response._id, req.body.username)

            res.status(201).json({
                message: "User successfully created!",
                result: response
            });
        }).catch(error => {
            res.status(500).json({
                error: error
            });
        });
    });
}

async function registerNeo4j(objectid, username){
    const session = neo.session()

    console.log(objectid, username)
    try{
        const resultNeo = await session.run(neo.createUser, {
            objectid: objectid.toString(),
            username: username.toString()
        })
    } catch(err){
        console.log(err)
    }
}

async function login(req, res) {
    if(!req.body) {
        throw new errors.EntityNotFoundError('User is required to provide login information.')
    }
    
    let getUser; 

    await userModel.findOne({
        email: req.body.email
    }).then(user => {
        if(user){
            getUser = user;
            return bcrypt.compare(req.body.password, user.password);
        }
    }).then(response => {
        if (!response) {
            return res.status(401).json({
                message: "Authentication failed."
            });
        }
        let jwtToken = jwt.sign({
            email: getUser.email,
            userId: getUser._id
        }, "longer-secret-is-better", {
            expiresIn: "1h"
        });
        res.status(200).json({
            token: jwtToken,
            expiresIn: 3600,
            _id: getUser._id
        });
    }).catch(err => {
        console.log(err)
        return res.status(401).json({
            message: "Authentication failed2."
        });
    });
}

async function deleteUser (req, res) {
    const authHeader = req.headers.authorization

    const token = authHeader.substring(7, authHeader.length)

    var decoded = jwt.decode(token, {complete: true});

    var user = await User.findById(req.params.id)

    if(!user){
        res.status(400).json({
            message: "Invalid resource id: the user you are looking for does not exist."
        })
    }

    if(user._id == decoded.payload.userId) {
        await user.delete()
        res.status(200).json({
            message: "User successfully deleted!",
        });
    }else {
        res.status(403).end()
    }
}

module.exports = {
    register,
    login,
    deleteUser
}