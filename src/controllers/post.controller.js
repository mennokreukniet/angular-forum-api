const Post = require('../models/post.model')() 
const Sub = require('../models/sub.model')() 
const Comment = require('../models/comment.model')()
const jwt = require("jsonwebtoken")

const errors = require('../errors')

function create(req, res) {

    const newPost = new Post({
        title: req.body.title,
        text: req.body.text,
        imagelink: req.body.imagelink,
        hyperlink: req.body.hyperlink,
        creationdate: Date.now(),
        likes: 0,
        originalPoster: req.body.originalPoster,
        sub: req.params.id
    });

    newPost.save().then((response) => {
        res.status(201).json({
            message: "Post successfully created!",
            result: response
        });
    }).catch(error => {
        res.status(500).json({
            error: error
        });
    });
}

async function update (req, res) {
    if(!req.body){
        res.status(400).end()
    }
    const authHeader = req.headers.authorization

    const token = authHeader.substring(7, authHeader.length)

    var decoded = jwt.decode(token, {complete: true});

    const post = await Post.findById(req.params.id)
    if(post.originalPoster == decoded.payload.id){
        await Post.findByIdAndUpdate(req.params.id, req.body)
        res.status(200).json({
            message: "Post successfully edited!",
        });
    }else{
        res.status(403).end()
    }
}

async function deletePost (req, res) {
    const authHeader = req.headers.authorization

    const token = authHeader.substring(7, authHeader.length)

    var decoded = jwt.decode(token, {complete: true});

    var post = await Post.findById(req.params.id)

    if(!post){
        res.status(400).json({
            message: "Invalid resource id: the resource you are looking for does not exist."
        })
    }

    if(post.originalPoster == decoded.payload.userId) {
        await post.delete()
        res.status(200).json({
            message: "Post successfully deleted!",
        });
    }else {
        res.status(401).end()
    }
}

module.exports = {
    create,
    update,
    deletePost,
}