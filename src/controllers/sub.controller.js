const Post = require('../models/post.model')() 
const Sub = require('../models/sub.model')() 
const { JsonWebTokenError } = require('jsonwebtoken');
const jwt = require("jsonwebtoken")
const neo = require("../../neo");


const errors = require('../errors')

async function getPosts(req, res) {
    Post.find({
        sub: req.params.id
    }).lean().exec((err, posts) => {
        if (err) {
            throw err;
        }
        res.status(200).json({
            posts: posts
        })
    });
}

function createSub(req, res) {
    if(!req.body){
        res.status(400).end()
    }

    // const rules = []

    // var i;
    
    // for (i = 0; i < req.body.rules.length; i++) {
    //     rules.push(req.body.rules[i])
    // }

    const newSub = new Sub({
        name: req.body.name,
        about: req.body.about,
        rules: req.body.rules,
        admin: req.body.admin
    });

    newSub.save().then((response) => {
        res.status(201).json({
            message: "Subforum successfully created!",
            result: response
        });
        createNeo4j(response._id, req.body.name)
    }).catch(error => {
        // res.status(500).json({
        //     error: error
        // });
    });
}

async function createNeo4j(objectid, name){
    const session = neo.session()

    try{
        const resultNeo = await session.run(neo.createSub, {
            objectid: objectid.toString(),
            name: name.toString()
        })
    } catch(err){
        console.log(err)
    }
}

async function update (req, res) {
    const authHeader = req.headers.authorization

    const token = authHeader.substring(7, authHeader.length)

    var decoded = jwt.decode(token, {complete: true});

    const sub = await Sub.findById(req.params.id)

    if(sub.admin == decoded.payload.userId){
        await Sub.findByIdAndUpdate(req.params.id, req.body)
        res.status(200).json({
            message: "Subforum successfully edited!",
        });
    }else{
        res.status(403).end()
    }
}


async function deleteSub (req, res) {
    const authHeader = req.headers.authorization

    const token = authHeader.substring(7, authHeader.length)

    var decoded = jwt.decode(token, {complete: true});

    var sub = await Sub.findById(req.params.id)

    if(!sub){
        res.status(400).json({
            message: "Invalid resource id: the resource you are looking for does not exist."
        })
    }

    if(sub.admin == decoded.payload.userId) {
        await sub.delete()
        res.status(200).json({
            message: "Subforum successfully deleted!",
        });
    }else {
        res.status(403).end()
    }
}

async function followSub (req, res) {
    const authHeader = req.headers.authorization

    const token = authHeader.substring(7, authHeader.length)

    var decoded = jwt.decode(token, {complete: true});

    if (!req.params.id || !decoded.payload.userId) {
        res.status(403).send({
            error: "userid and or subid was not provided.",
        });
    } else {
        try {
            const session = neo.session();

            const result = await session.run(neo.createFollow, {
                userid: decoded.payload.userId,
                subid: req.params.id,
            });

            res.status(201).send(result);
            session.close();
        } catch (err) {
            return res.status(400).json({
                message: "follow sub failed",
            });
        }
    }
}

async function unfollowSub (req, res) {
    const authHeader = req.headers.authorization

    const token = authHeader.substring(7, authHeader.length)

    var decoded = jwt.decode(token, {complete: true});

    if (!req.params.id || !decoded.payload.userId) {
      res.status(403).send({
        error: "userid and or subid was not provided.",
      });
    } else {
      try {
            const session = neo.session();

            const result = await session.run(neo.removeFollow, {
                userid: decoded.payload.userId,
                subid: req.params.id,
            });

            res.status(200).send(result);
            session.close();
        } catch (err) {
            console.log(err);
            return res.status(403).json({
                message: "unfollow sub failed",
            });
        }
    }
}

async function getFollowedSubs (req, res) {
    if (!req.params.id) {
        res.status(401).send({
          error: "No user id was provided.",
        })
      } else {
        try {
            const session = neo.session();

            const result = await session.run(neo.getFollowing, {
                userid: req.params.id
            });

            const objectids = result.records[0].get("objectIds");
            const followingSubs = await Sub.find({
              _id: { $in: objectids },
            });

            res.status(200).send(followingSubs);
            session.close();
        } catch (err) {
            return res.status(403).json({
                message: "Bad request",
            })
        }
    }
}

module.exports = {
    getPosts,
    update,
    createSub,
    deleteSub,
    followSub,
    unfollowSub,
    getFollowedSubs
}