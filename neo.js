const neo4j = require('neo4j-driver')

function connect(dbName) {
    this.dbName = dbName
    this.driver = neo4j.driver(
        process.env.NEO4J_URL,
        neo4j.auth.basic(process.env.NEO4J_USER, process.env.NEO4J_PASSWORD)
    )
}

function session() {
    return this.driver.session({
        database: this.dbName,
        defaultAccessMode: neo4j.session.WRITE
    })
}

module.exports = {
    connect,
    session,
    dropAll: "MATCH (n) DETACH DELETE n",
    getUser: "MATCH (n:User {objectid: $id}) RETURN n.username",
    getSub: "MATCH (n:Sub {objectid: $id}) RETURN n.name",
    createSub: "CREATE (u:Sub {objectid: $objectid, name: $name}) RETURN u",
    createUser: "CREATE (u:User {objectid: $objectid, name: $username}) RETURN u",
    createFollow: "MATCH (u:User {objectid: $userid})  MATCH (s:Sub {objectid: $subid}) CREATE (u)-[r:FOLLOWING]->(s)",
    removeFollow: "MATCH (:User {objectid: $userid})-[r:FOLLOWING]-(:Sub {objectid: $subid}) DELETE r",
    getFollowing: "MATCH (a:User {objectid: $userid})-[r]-(b) RETURN collect(DISTINCT b.objectid) as objectIds",
  };